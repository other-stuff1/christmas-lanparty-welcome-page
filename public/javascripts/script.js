
const cod2MapVsetinButton = document.getElementById("cod2MapVsetinBut");
const cod2MapPavlovButton = document.getElementById("cod2MapPavlovBut");
const cod2MapZamekButton = document.getElementById("cod2MapZamekBut");
const cod2GameButton = document.getElementById("cod2GameBut");
const cod2PatchButton = document.getElementById("cod2PatchBut");

const cod4GameButton = document.getElementById("cod4GameBut");
// const cod4PatchButton1 = document.getElementById("cod4PatchBut1");
// const cod4PatchButton2 = document.getElementById("cod4PatchBut2");

const bf2GameButton = document.getElementById("bf2GameBut");
const bf2PatchButton = document.getElementById("bf2PatchBut");
const bf2CrackButton = document.getElementById("bf2CrackBut");

const trackmaniaGameButton = document.getElementById("trackmaniaGameBut");
const smokinGunsGameButton = document.getElementById("smokinGameBut");

const chatInputNick = document.getElementById("chat-input-nick");
const chatInputContent = document.getElementById("chat-input-content");
const chatButton = document.getElementById("chat-button");
const colorButton = document.getElementById("colorBut");
const setButton = document.getElementById("setBut");
const resetButton = document.getElementById("resetBut");
const chatInputLabel = document.getElementById("chat-input-label");
const emojiHint = document.getElementById("emoji-hint");
const notifyCheckbox = document.getElementById("notifyCheckbox");
const notifyCheckboxWrapper = document.getElementById("notifyCheckboxWrapper");

const colorWheelEl = document.getElementById("colorWheelDemo");

let notify = false;
let openColorPicker = false;
let chosenColor = "cadetblue";

const colorWheel = iro.ColorPicker("#colorWheelDemo", () => {
});

colorWheel.on('color:change', function(color, changes){
   chatInputNick.style['background-color'] = color.hexString;
   chatInputContent.style['background-color'] = color.hexString;
    chatInputNick.style['color'] = "#fff";
    chatInputContent.style['color'] = "#fff";
    emojiHint.style["color"] = "#fff";
   chosenColor = color.hexString;
});

function getCookie(cname) {
    const name = cname + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const init = () => {
  if(getCookie("nick")){
      chatInputNick.value = getCookie("nick");
      chatInputNick.disabled = true;
      chatInputNick.style['opacity'] = 0.7;
      chosenColor = getCookie("color");

      emojiHint.style["color"] = "#000";
      emojiHint.style["display"] = "block";

      let message = getCookie("message");
      if(message) {
          chatInputContent.value = message;
      }

      if(chosenColor !== "cadetblue"){
        chatInputNick.style['background-color'] = getCookie("color");
        chatInputContent.style['background-color'] = getCookie("color");
        chatInputNick.style['color'] = "#fff";
        chatInputContent.style['color'] = "#fff";
        emojiHint.style["color"] = "#fff";
      }
      colorButton.style["display"] = "none";
      setButton.style["display"] = "none";
      chatButton.style["display"] = "block";
      chatInputContent.style["display"] = "block";
      chatInputLabel.style["display"] = "block";

      notifyCheckboxWrapper.style["display"] = "flex";

      notify = (getCookie("notify") === "true");
      notifyCheckbox.checked = notify;
      if(notify) {
          chatInputContent.focus();
      }
  } else {
      resetButton.style["display"] = "none";
  }
};
init();

const chatWindow = document.getElementById("chat-window");
chatWindow.scrollTop = chatWindow.scrollHeight;

window.addEventListener("keydown", (event) => {
    if (event.code == "Enter") {
        if(getCookie("nick") && getCookie("color")) {
            sendMessage();
        }
    }
});

// SOCKET IO
const socket = io();

socket.on("reload", (data) => {
    if(notify || data.server === true) {
        if(chatInputContent.value && chatInputContent.value !== ""){
            document.cookie = `message=${chatInputContent.value}`;
        }
        location.reload();
    }
});

cod2MapVsetinButton.addEventListener("click", () => {
   window.open(`http://${window.location.hostname}:${window.location.port}/download/cod2MapVsetin`, '_blank');
});
cod2MapPavlovButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/cod2MapPavlov`, '_blank');
});
cod2MapZamekButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/cod2MapZamek`, '_blank');
});
cod2GameButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/cod2Game`, '_blank');
});
cod2PatchButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/cod2Patch`, '_blank');
});


cod4GameButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/cod4Game`, '_blank');
});
// cod4PatchButton1.addEventListener("click", () => {
//     window.open(`http://${window.location.hostname}:${window.location.port}/download/cod4Patch1`, '_blank');
// });
// cod4PatchButton2.addEventListener("click", () => {
//     window.open(`http://${window.location.hostname}:${window.location.port}/download/cod4Patch2`, '_blank');
// });

bf2GameButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/bf2Game`, '_blank');
});
bf2PatchButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/bf2Patch`, '_blank');
});
bf2CrackButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/bf2Crack`, '_blank');
});


trackmaniaGameButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/trackmaniaGame`, '_blank');
});

smokinGunsGameButton.addEventListener("click", () => {
    window.open(`http://${window.location.hostname}:${window.location.port}/download/smokinGunsGame`, '_blank');
});

chatInputContent.addEventListener("focus", () => {
    emojiHint.style["display"] = "block";
});
chatInputContent.addEventListener("focusout", () => {
    emojiHint.style["display"] = "none";
});


colorButton.addEventListener("click", () => {
    colorWheelEl.style["display"] = !openColorPicker ? "block" : "none";
    openColorPicker = !openColorPicker;
});

notifyCheckbox.addEventListener("click", () => {
    notify = !notify;
    notifyCheckbox.checked = notify;
    document.cookie = `notify=${notify}`;
    if(notify) {
        location.reload();
    }
});

setButton.addEventListener("click", () => {
    document.cookie = `nick=${chatInputNick.value}`;
    document.cookie = `color=${chosenColor}`;
    document.cookie = `notify=${true}`;
    setButton.style["display"] = "none";
    resetButton.style["display"] = "block";
    colorButton.style["display"] = "none";
    chatButton.style["display"] = "block";
    openColorPicker = false;
    colorWheelEl.style["display"] = "none";
    chatInputContent.style["display"] = "block";
    chatInputLabel.style["display"] = "block";
    chatInputNick.style['opacity'] = 0.7;
    chatInputNick.disabled = true;
    notify = true;
    notifyCheckbox.checked = notify;
    notifyCheckbox.style["display"] = "block";
    notifyCheckboxWrapper.style["display"] = "flex";
    location.reload();
});

resetButton.addEventListener("click", () => {
    document.cookie = "nick=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    document.cookie = "color=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    document.cookie = "notify=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    setButton.style["display"] = "block";
    resetButton.style["display"] = "none";
    chatInputContent.style["display"] = "none";
    chatInputLabel.style["display"] = "none";
    colorButton.style["display"] = "block";
    chatButton.style["display"] = "none";
    openColorPicker = false;
    colorWheelEl.style["display"] = "none";
    chatInputNick.style['opacity'] = 1;
    chatInputNick.disabled = false;
    chatInputNick.style['background-color'] = "white";
    chatInputNick.style['color'] = "black";
    chatInputContent.style['background-color'] = "white";
    chatInputContent.style['color'] = "black";
    chosenColor = "cadetblue";
    notify = false;
    notifyCheckbox.checked = notify;
    notifyCheckbox.style["display"] = "none";
    notifyCheckboxWrapper.style["display"] = "none";
    emojiHint.style["display"] = "none";
    // location.reload();
});

chatButton.addEventListener("click", () => {
    sendMessage();
});

const checkEmpty = (str) => {
    if (!str.replace(/\s/g, '').length) {
        return true;
    } else {
        return false;
    }
};

const sendMessage = () => {
    if(chatInputNick.value && chatInputNick.value !== "" && chatInputContent.value && chatInputContent.value !== "" && !checkEmpty(chatInputContent.value)) {
        fetch(`/chat`,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    nick: chatInputNick.value,
                    content: chatInputContent.value,
                    color: chosenColor
                })
            }).then(function (res) {
            return res.json();
        })
            .then(function (data) {
                alert(JSON.stringify(data))
            });

        notify = true;
        notifyCheckbox.checked = notify;
        document.cookie = `notify=${notify}`;
        chatInputContent.value = "";
        document.cookie = "message=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    }
};
