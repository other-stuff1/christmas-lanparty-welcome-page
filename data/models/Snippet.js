"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DataStore_1 = require("../DataStore");
var Snippet = /** @class */ (function () {
    function Snippet(data) {
        this.id = DataStore_1.getID();
        this.nick = data.nick;
        this.content = data.content;
        this.color = data.color;
    }
    return Snippet;
}());
exports.Snippet = Snippet;
