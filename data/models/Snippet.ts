import {getID} from "../DataStore";

export class Snippet {
    id: number;
    nick: any;
    content: any;
    color: any;
    constructor(data: any) {
        this.id = getID();
        this.nick = data.nick;
        this.content = data.content;
        this.color = data.color;
    }
}
