const jsonStore = require("./store.json");

export let ID = 0;

export const getID = () => {
  return ID++;
};

export class DataStore {
    static store = jsonStore;
}