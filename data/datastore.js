"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var jsonStore = require("./store.json");
exports.ID = 0;
exports.getID = function () {
    return exports.ID++;
};
var DataStore = /** @class */ (function () {
    function DataStore() {
    }
    DataStore.store = jsonStore;
    return DataStore;
}());
exports.DataStore = DataStore;
