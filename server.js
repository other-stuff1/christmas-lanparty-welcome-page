"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var bodyParser_1 = require("./bodyParser");
var Snippet_1 = require("./data/models/Snippet");
var http = require('http');
var path = require('path');
var fs = require('fs');
var port = process.env.PORT || 2019;
var app = express_1.default();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express_1.default.static(__dirname + '/public'));
app.get('/', function (req, res, next) {
    res.render('index', { title: 'Vanocni LAN party 2019', snippets: loadData() });
});
/**
 * COD 2
 */
app.get('/download/cod2MapVsetin', function (req, res, next) {
    res.download('public/download/mp_vsetin.iwd', 'mp_vsetin.iwd');
});
app.get('/download/cod2MapPavlov', function (req, res, next) {
    res.download('public/download/mp_pavlov.iwd', 'mp_pavlov.iwd');
});
app.get('/download/cod2MapZamek', function (req, res, next) {
    res.download('public/download/mp_vsetin_palace.iwd', 'mp_vsetin_palace.iwd');
});
app.get('/download/cod2Game', function (req, res, next) {
    res.download('public/download/cod2.iso', 'cod2.iso');
});
app.get('/download/cod2Patch', function (req, res, next) {
    res.download('public/download/cod2-v1.3.zip', 'cod2-v1.3.zip');
});
/**
 * COD 4
 */
app.get('/download/cod4Game', function (req, res, next) {
    res.download('public/download/cod4.zip', 'cod4.zip');
});
// app.get('/download/cod4Patch1',(req: any, res: any, next: any) => {
//     res.download('public/download/cod4/cod4-v1.3.zip', 'cod2-v1.3.zip');
// });
// app.get('/download/cod4Patch2',(req: any, res: any, next: any) => {
//     res.download('public/download/cod4/cod4-v1.3.zip', 'cod2-v1.3.zip');
// });
/**
 * BF 2
 */
app.get('/download/bf2Game', function (req, res, next) {
    res.download('public/download/bf2/bf2.iso', 'bf2.iso');
});
app.get('/download/bf2Patch', function (req, res, next) {
    res.download('public/download/bf2/BF2_patch_1.41.exe', 'BF2_patch_1.41.exe');
});
app.get('/download/bf2Crack', function (req, res, next) {
    res.download('public/download/bf2/BF2_crack_1.41.rar', 'BF2_crack_1.41.rar');
});
/**
 * Trackmania
 */
app.get('/download/trackmaniaGame', function (req, res, next) {
    res.download('public/download/TMNF_setup.exe', 'TMNF_setup.exe');
});
/**
 * Smokin guns
 */
app.get('/download/smokinGunsGame', function (req, res, next) {
    res.download('public/download/Smokin_Guns_1.1.exe', 'Smokin_Guns_1.1.exe');
});
app.post('/chat', bodyParser_1.jsonParser, function (req, res, next) {
    var content = req.body;
    console.log(content);
    var data = loadData();
    var snipp = new Snippet_1.Snippet(content);
    data.push(snipp);
    storeData(data);
    // console.log(loadData());
    io.emit("reload");
});
var storeData = function (data) {
    try {
        fs.writeFileSync("data/store.json", JSON.stringify(data));
    }
    catch (err) {
        console.error(err);
    }
};
var loadData = function () {
    try {
        return JSON.parse(fs.readFileSync("data/store.json", 'utf8'));
    }
    catch (err) {
        console.error(err);
        return false;
    }
};
var server = http.createServer(app);
// SOCKET IO
var socketIo = require('socket.io');
var io = socketIo(server);
io.on('connection', function (socket) {
    console.log("New client connected " + socket.id);
});
// START SERVER
server.listen(port, function () {
    console.log("Listening on port " + port);
    setTimeout(function () { return io.emit("reload", { server: true }); }, 2000);
});
