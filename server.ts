import express from 'express';
import {DataStore} from "./data/DataStore";
import {jsonParser} from "./bodyParser";
import {Snippet} from "./data/models/Snippet";
const http = require('http');
const path = require('path');

import ejs from "ejs";

const fs = require('fs');

const port = process.env.PORT || 2019;

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.get('/',(req: any, res: any, next: any) => {
    res.render('index', { title: 'Vanocni LAN party 2019', snippets: loadData() } );
});

/**
 * COD 2
 */
app.get('/download/cod2MapVsetin',(req: any, res: any, next: any) => {
   res.download('public/download/mp_vsetin.iwd', 'mp_vsetin.iwd');
});
app.get('/download/cod2MapPavlov',(req: any, res: any, next: any) => {
    res.download('public/download/mp_pavlov.iwd', 'mp_pavlov.iwd');
});
app.get('/download/cod2MapZamek',(req: any, res: any, next: any) => {
    res.download('public/download/mp_vsetin_palace.iwd', 'mp_vsetin_palace.iwd');
});
app.get('/download/cod2Game',(req: any, res: any, next: any) => {
    res.download('public/download/cod2.iso', 'cod2.iso');
});
app.get('/download/cod2Patch',(req: any, res: any, next: any) => {
    res.download('public/download/cod2-v1.3.zip', 'cod2-v1.3.zip');
});

/**
 * COD 4
 */
app.get('/download/cod4Game',(req: any, res: any, next: any) => {
    res.download('public/download/cod4.zip', 'cod4.zip');
});
// app.get('/download/cod4Patch1',(req: any, res: any, next: any) => {
//     res.download('public/download/cod4/cod4-v1.3.zip', 'cod2-v1.3.zip');
// });
// app.get('/download/cod4Patch2',(req: any, res: any, next: any) => {
//     res.download('public/download/cod4/cod4-v1.3.zip', 'cod2-v1.3.zip');
// });

/**
 * BF 2
 */
app.get('/download/bf2Game',(req: any, res: any, next: any) => {
    res.download('public/download/bf2/bf2.iso', 'bf2.iso');
});
app.get('/download/bf2Patch',(req: any, res: any, next: any) => {
    res.download('public/download/bf2/BF2_patch_1.41.exe', 'BF2_patch_1.41.exe');
});
app.get('/download/bf2Crack',(req: any, res: any, next: any) => {
    res.download('public/download/bf2/BF2_crack_1.41.rar', 'BF2_crack_1.41.rar');
});

/**
 * Trackmania
 */
app.get('/download/trackmaniaGame',(req: any, res: any, next: any) => {
    res.download('public/download/TMNF_setup.exe', 'TMNF_setup.exe');
});

/**
 * Smokin guns
 */
app.get('/download/smokinGunsGame',(req: any, res: any, next: any) => {
    res.download('public/download/Smokin_Guns_1.1.exe', 'Smokin_Guns_1.1.exe');
});


app.post('/chat',jsonParser,(req: any, res: any, next: any) => {
    const content = req.body;
    console.log(content);

    const data = loadData();

    const snipp = new Snippet(content);
    data.push(snipp);

    storeData(data);

    // console.log(loadData());

    io.emit("reload");
});

const storeData = (data: any) => {
    try {
        fs.writeFileSync("data/store.json", JSON.stringify(data));
    } catch (err) {
        console.error(err);
    }
};

const loadData = () => {
    try {
        return JSON.parse(fs.readFileSync("data/store.json", 'utf8'));
    } catch (err) {
        console.error(err);
        return false;
    }
};


const server = http.createServer(app);

// SOCKET IO
const socketIo = require('socket.io');

const io = socketIo(server);

io.on('connection', function(socket: any){
    console.log(`New client connected ${socket.id}`);
});


// START SERVER
server.listen(port, () => {
    console.log(`Listening on port ${port}`);
    setTimeout(() =>  io.emit("reload", {server: true}),2000);
});